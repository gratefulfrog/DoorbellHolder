$fn= 100;

DXFFileName = "DoorbellHolder_V3.dxf";

layer5mm  = "Up5mm";
layer8mm  = "Up8mm";
layer30mm = "Up30mm";

DXFCurveFIleName = "baseCurveGoodToGo.dxf";
curveLayerInner = "0";
curveLayerOuter = "offset";

module baseCurveInner(h=10){
  translate([80,0,0])
    rotate([0,-90,-90])
    translate([5,0,-10])
      linear_extrude(h,$fn=100)
        import(DXFCurveFIleName,layer=curveLayerInner,$fn=100);
}
%baseCurveInner();
//half();

module baseCurveOuter(d=5){
  translate([0,-20+d,0])
    baseCurveInner(d);
}
%baseCurveOuter();

module curvedSide(d=3){
  h=3;
  translate([0,-18,0])
    translate([80,0,0])
      rotate([0,-90,-90])
      translate([5,0,-10])
        linear_extrude(h,$fn=100)
          import(DXFCurveFIleName,layer=curveLayerOuter,$fn=100);
}
%curvedSide();

module half(){
    union(){
      linear_extrude(5)
        import(DXFFileName,layer=layer5mm);
      linear_extrude(8)
        import(DXFFileName,layer=layer8mm);
      linear_extrude(30)
        import(DXFFileName,layer=layer30mm);
      curvedSide();
      baseCurveInner();
      baseCurveOuter();
    }
}
//projection(true)
//translate([0,0,26])
//rotate([90,0,0])
%half();
module otherHalf(){
  mirror([0,1,0])
    half();
}
%otherHalf();

module bothHalvesLaidOut(){
  translate([80,0,0])
    rotate([0,0,180])
      half();
  translate([-10,-35,0])
    otherHalf();
}
bothHalvesLaidOut();

module fullHolder(){
  translate([0,-70,60])
    rotate([180,0,0])
      half();
  translate([0,-70,0])
    otherHalf();
}
//translate([0,42,0])
//fullHolder();

///////////////////////// out cuts /////////////////////

//layer30Cmm = "curve";
//layer8mmBis = "otherCurve";
/*
module baseCurveInnerOld(){
  translate([80,-10,0])
    rotate([-90,0,0])
      mirror([1,0,0])
      linear_extrude(10,$fn=100)
        import(DXFFileName,layer=layer8mmBis,$fn=100);
        //import(DXFCurveFIleName,$fn=100);

}
//%baseCurveInnerOld();
*/
/*
import(DXFFileName,layer=layer8mmBis,$fn=100);
import(DXFCurveFIleName,$fn=100);
*/
//baseCurveInner();
/*
module baseCurveOuterOld(){
  translate([0,-20,0])
    baseCurveInnerOld();
}
//baseCurveOuterOld();
*/

/*
module curvedSideOld(){
  translate([0,-30,0])
    rotate([-90,0,0])
      linear_extrude(30,$fn=100)
        import(DXFFileName,layer=layer30mm,$fn=100);
}
*/

/*
module topCutter(){
  z= 25+5;
  x = 200;
  y = 200;
  h= 20;
  translate([-x/2.,-y/2.,z])
    cube([x,y,h]);
}
//topCutter();
*/