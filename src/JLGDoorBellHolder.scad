$fn=100;

// Rail dimensions
railY   = 35.5;
railZ   = 90.5;
railX   =  100;
railGap = 1.5;  // total 

buzzerBorder    = 1.5;
buzzerBorderY   = 1.5;
buzzerZRaw      = 84;
buzzerZ         = buzzerZRaw + 2*buzzerBorder;
buzzerLoopZ     = 3;
buzzerXRaw      = 40;
buzzerX         = buzzerXRaw  + 2*buzzerBorder;
buzzerY         = 16;
buzzerButtonDia = 27.5;
buzzerButtonY   = 1;
buzzerGap       = 0.75;  // per side

wallYThickness           = 5;
wallXThickness           = 5.5;
wallZThickness           = wallYThickness;
hookY                    = 3;
doubleSidedTapeThickness = 2.5;
hookYGap                 = 0.75;
hookXGap                 = 0.75;
hookOverlap              = 4 + buzzerBorder;

holeOverlap = 2; // each side

/* layout ordering */
/* Y order :  */
totalY  =  hookY 
          + hookYGap 
          + buzzerY 
          + doubleSidedTapeThickness 
          + wallYThickness 
          + railGap/2. 
          + railY 
          + railGap/2.
          + wallYThickness;

/* X Order: */
totalX =   wallXThickness
          + hookXGap 
          + buzzerX
          + hookXGap 
          + wallXThickness;

/* Hook X Order: */
hookTotalX =   wallXThickness
              + hookOverlap
              + totalX -2*(wallXThickness + hookOverlap)
              + hookOverlap
              + wallXThickness;

/* Z order */
totalZ =   wallYThickness
          + buzzerZ
          //+ buzzerGap
          + wallYThickness;

module buzzer(){
  translate([0,-buzzerY/2./*-(railGap/2.-wallYThickness-doubleSidedTapeThickness)*/,0]){
  color("blue")
  cube([buzzerXRaw,buzzerY,buzzerZRaw],center=true);
  cube([buzzerX,buzzerBorderY,buzzerZ],center=true);
  translate([0,-buzzerButtonY,0])
    rotate([90,0,0])
      cylinder(h=buzzerY,d=buzzerButtonDia,center=true);
  }
  doubleSidedTape();
}
translate([0,-buzzerY/2.,-buzzerZ/2.+railZ/2.])
  %buzzer();

module doubleSidedTape(){
  color("green")
    translate([0,doubleSidedTapeThickness/2.,0])
    cube([buzzerXRaw,doubleSidedTapeThickness,buzzerZRaw],center=true);
}
//doubleSidedTape();

module rail(){
  color("red")
  cube([railX,railY,railZ],center=true);
}
translate([0,railY/2.,0])
  %rail();

module backPanel(){
  translate([0,wallYThickness/2.+railY+railGap/2.,(railZ/2.+wallZThickness)/2.])
    cube([totalX,wallYThickness,railZ/2.+wallZThickness],center = true);
}
//backPanel();

module topPanel(){
  Y= railY+railGap+2*wallYThickness;
  translate([0,-Y/2.+railY+railGap/2.+wallYThickness,wallZThickness/2.+railZ/2.])
  cube([totalX,Y,wallZThickness],center=true);
}
//topPanel();

module middlePanel(){
  translate([0,-wallYThickness/2.-railGap/2.,-totalZ/2.+railZ/2.+wallZThickness])
    cube([totalX,wallYThickness,totalZ],center=true);
}
//middlePanel();

module bottomPanel(){
  Y = totalY -railY- railGap- wallYThickness;
  difference(){
    translate([0,-Y/2.-railGap/2.,-wallZThickness/2.+railZ/2.+wallZThickness-totalZ+wallZThickness])
      cube([totalX,Y,wallZThickness],center=true);
    holeCutter();
  }
}
//bottomPanel();
  
module sidePanel(){
  Y = totalY -railY- railGap- wallYThickness;
  translate([0,-Y/2-railGap/2.,-totalZ/2+railZ/2+wallZThickness])
    cube([wallXThickness,Y,totalZ],center =true);
}
//%sidePanel();

module sidePanels(){
  translate([-wallXThickness/2+totalX/2.,0,0])
    sidePanel();
  translate([wallXThickness/2-totalX/2.,0,0])
    sidePanel();
}
//sidePanels();

module horizontalHook(){
  Z= wallZThickness+hookOverlap;
  yTrans= -hookY/2. - railGap/2. - wallYThickness-doubleSidedTapeThickness-buzzerY-hookYGap;
  translate([0,yTrans,-Z/2.+railZ/2.+wallZThickness-totalZ+Z]) // -railZ/2.-wallZThickness])
    cube([totalX,hookY,Z],center=true);
}
//horizontalHook();

module hookColumn(){
  X=wallXThickness+hookOverlap;
  yTrans= -hookY/2. - railGap/2. - wallYThickness-doubleSidedTapeThickness-buzzerY-hookYGap;
  translate([0,yTrans,-totalZ/2.+railZ/2.+wallZThickness])
    cube([X,hookY,totalZ],center=true);
}
//hookColumn();
module hooks(){
  X=wallXThickness+hookOverlap;
  translate([-X/2.+totalX/2.,0,0])
    hookColumn();
  translate([X/2.-totalX/2.,0,0])
    hookColumn();
  horizontalHook();
}
//hooks();

module holeCutter(){
  diaY = buzzerY + doubleSidedTapeThickness-2*holeOverlap;
  diaX = buzzerX - 2*holeOverlap;
  translate([0,-railGap/2.-wallYThickness-(doubleSidedTapeThickness+buzzerY)/2.])
    resize([diaX,0,0],false)
      cylinder(d=diaY, h = 100, center = true);
}
//holeCutter();

module all(){
  backPanel();
  topPanel();
  middlePanel();
  bottomPanel();
  sidePanels();
  hooks();
}
//all();

module leftRight(left){
  size = 200;
  difference(){
    all();
    if(left){
      translate([size/2.,0,0])
        cube(size,center=true);
    }
    else{
      translate([-size/2.,0,0])
        cube(size,center=true);
    }
  }
}
module left(){
  leftRight(true);
}
module right(){
  leftRight(false);
}
left();
translate([20,0,0])
right();