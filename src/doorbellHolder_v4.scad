$fn= 100;

DXFFileName = "DoorbellHolder_V4.dxf";

layer5mm  = "Up5mm";
layer8mm  = "Up8mm";
layer30mm = "Up30mm";

DXFCurveFIleName = "baseCurveGoodToGo_v2.dxf";
curveLayerInner = "0";
curveLayerOuter = "offset";
sidePlateLayer  = "sidePlate";

module holeCutter(){
  translate([85,-15,30])
    rotate([0,90,0])
      scale([1.2,1,1])
        cylinder(h=20,r=10,center=true);
}

module baseCurveInner(h=28){ 
  rotate([-90,0,0])
    translate([0,0,-h])
      linear_extrude(h,$fn=100)
          import(DXFFileName,layer=curveLayerInner,$fn=100);
}
module sidePlater(h=5){ 
  rotate([-90,0,0])
    translate([0,0,-h])
      linear_extrude(h,$fn=100)
        import(DXFFileName,layer=sidePlateLayer,$fn=100);
}
%sidePlater();

/*
module baseCurveInner(h=10){
  translate([80,0,0])
    rotate([0,-90,-90])
    translate([5,0,-10])
      linear_extrude(h,$fn=100)
        import(DXFCurveFIleName,layer=curveLayerInner,$fn=100);
}
*/
%baseCurveInner();

module baseCurveOuter(d=5){
  translate([0,-20+d,0])
    baseCurveInner(d);
}
//%baseCurveOuter();

module curvedSide(d=3){
  h=3;
  translate([0,-18,0])
    translate([80,0,0])
      rotate([0,-90,-90])
      translate([5,0,-10])
        linear_extrude(h,$fn=100)
          import(DXFCurveFIleName,layer=curveLayerOuter,$fn=100);
}
%curvedSide();

module half(){
  difference(){
    union(){
      linear_extrude(5)
        import(DXFFileName,layer=layer5mm);
      linear_extrude(8)
        import(DXFFileName,layer=layer8mm);
      linear_extrude(30)
        import(DXFFileName,layer=layer30mm);
      curvedSide();
      sidePlater();
      baseCurveInner();
    }
    holeCutter();
  }
}
//projection(true)
//translate([0,0,26])
//rotate([90,0,0])
//half();
module otherHalf(){
  mirror([0,1,0])
    half();
}
//otherHalf();

module bothHalvesLaidOut(){
  translate([80,0,0])
    rotate([0,0,180])
      half();
  translate([-10,-35,0])
    otherHalf();
}
bothHalvesLaidOut();

module fullHolder(){
  translate([0,-70,60])
    rotate([180,0,0])
      half();
  translate([0,-70,0])
    otherHalf();
}
/*
//projection(true)
translate([0,0,1])
  rotate([90,0,0])
    translate([0,42,0])
      fullHolder();

*/